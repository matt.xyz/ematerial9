import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-material-escolar',
  templateUrl: './material-escolar.component.html',
  styleUrls: ['./material-escolar.component.css']
})
export class MaterialEscolarComponent implements OnInit {
  form!:FormGroup;
  arrayutiles:string[]=[];

  constructor(private fb: FormBuilder) { 
    this.Formulario();
  }

  ngOnInit(): void {
  }

  get invalido(){
    return this.form.invalid;
  }

  get getUtiles(){
    return this.form.get('utiles') as FormArray;
  }
  
  Formulario(){
   
    this.form = this.fb.group({      
       utiles: this.fb.array([])
    })
    this.getUtiles.push(this.fb.group({
      check: [null],
      utilNuevo : [null,Validators.pattern(/^[1-9-0-a-zA-zñÑ\s]+$/)]
    }));
  }

  //En conjunto:
  deleteAll(){
    this.arrayutiles=[];
    this.getUtiles.clear()
  }

  addMaterial():void{
    const nuevo = this.fb.group({
      check: [null],
      utilNuevo : [null,Validators.pattern(/^[0-9a-zA-zñÑ\s]+$/)]
    })
    this.getUtiles.insert(0,nuevo);
  }

  deleteUtilEscolar(id:number){
    this.getUtiles.removeAt(id);
  }

  guardar(){
    this.arrayutiles=[];
    for (let i = 0; i < this.getUtiles.length; i++) {
      if (this.getUtiles.at(i).get('check')?.value ==true &&  this.getUtiles.at(i).get('utilNuevo')?.value != null ) {
        this.arrayutiles.push( this.getUtiles.at(i).get('utilNuevo')?.value);
      }  
    }
    console.log(this.arrayutiles);
    
  }

  limpiar():void{
    for (let i = 0; i < this.getUtiles.length; i++) {
       this.getUtiles.at(i).get('utilNuevo')?.setValue(null);
       this.getUtiles.at(i).get('check')?.setValue(null);
       
    }
    this.form.get('utiles')?.reset;
  }
}